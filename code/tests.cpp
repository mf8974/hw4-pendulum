#include <stdio.h>
#include "pendulum.h"

// NOTE(miha): This unit testing library is copied from:
// https://jera.com/techinfo/jtns/jtn002.
#define mu_assert(message, test) do { if (!(test)) return message; } while (0)
#define mu_run_test(test) do { char *message = test(); tests_run++; \
                               if (message) return message; } while (0)
extern int tests_run;
int tests_run = 0;

bool 
ApproxEquals(f64 Value, f64 Other, f64 Epsilon)
{
    return fabs(Value - Other) < Epsilon;
}

// NOTE(miha): We have not moving pendulum, so the positions should all be 0.
static char *
TestNotMovingPendulum()
{
    f64 *Positions = Pendulum(2.0f, 10.0f, ToRadians(0.0f), ToRadians(0.0f), 1000);

    for(u32 Index = 0; Index < 1000; ++Index)
    {
        mu_assert("Pendulum is not moving", ApproxEquals(Positions[Index], 0.0f, 0.000001));
    }

    return 0;
}

// NOTE(miha): We have a moving pendulum, so positions should all be != 0.
static char *
TestMovingPendulum()
{
    f64 *Positions = Pendulum(2.0f, 10.0f, ToRadians(1.0f), ToRadians(1.0f), 1000);

    for(u32 Index = 0; Index < 1000; ++Index)
    {
        mu_assert("Pendulum is moving", Positions[Index] != 0.0f);
    }

    return 0;

}

static char *
RunTests()
{
    mu_run_test(TestNotMovingPendulum);
    mu_run_test(TestMovingPendulum);

    return 0;
}

int
RunTestsAndStatistics()
{
    char *Result = RunTests();

    if(Result != 0)
    {
        printf("%s\n", Result);
    }
    else
    {
        printf("ALL TESTS PASSED\n");
    }
    
    printf("Tests run: %d\n", tests_run);

    return Result != 0;
}

int
main()
{
    RunTestsAndStatistics();

    return 0;
}
