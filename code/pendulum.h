#ifndef PENDULUM_H
#define PENDULUM_H

#include <stdlib.h>
#include <stdio.h>
#include <stdint.h>
#include <math.h>
#include <float.h>

#if DEBUG
#define Assert(Expression) if(!(Expression)) {*(int *)0 = 0;}
#else
#define Assert(Expression)
#endif

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef float f32;
typedef double f64;

typedef f64 (*function)(f64);

#define PI 3.14159265359f

#define ToRadians(Angle) ((Angle)*(PI/180.0f))

// NOTE(miha): First of the equations we got from transfering original second
// order diferential equation to the first order. This returns acceleration at
// angle 'Theta', 'G' and 'Length' are given in function 'Pendulum'.
inline f64
OmegaFunction(f64 Theta, f64 G, f64 Length)
{
    f64 Result = 0.0f;
    Result = -(G/Length)*sin(Theta);
    return Result;
}

// NOTE(miha): Second of the equations we got from transfering original second
// order diferential equation to the first order. This returns velocity at
// 'Omega'.
inline f64
ThetaFunction(f64 Omega)
{
    return Omega;
}


// NOTE(miha): Caluclate differential equation for undamped pendulum using
// fourth order Runga-Kutta method. 'Length' is the length of the pendulum,
// 'Time' is for how long pendulum swings, 'InitialTheta' is the initial
// position offset given in radians, 'InitialOmega' is the initial velocity of
// the pendulum and 'NumberOfIntervals' is how many intervals (precision) is
// there.
f64 *
Pendulum(f64 Length, f64 Time, f64 InitialTheta, f64 InitialOmega,
         u32 NumberOfIntervals)
{
    f64 Step = Time/NumberOfIntervals;
    f64 G = 9.80665f;

    f64 *OmegaValues = (f64 *) malloc(NumberOfIntervals * sizeof(f64));
    f64 *ThetaValues = (f64 *) malloc(NumberOfIntervals * sizeof(f64));

    OmegaValues[0] = InitialOmega;
    ThetaValues[0] = InitialTheta;

    for(u32 Index = 0; Index < NumberOfIntervals-1; ++Index)
    {
        f64 K1Omega = Step * OmegaFunction(ThetaValues[Index], G, Length);
        f64 K1Theta = Step * ThetaFunction(OmegaValues[Index]);
        f64 K2Omega = Step * OmegaFunction(ThetaValues[Index] + 0.5f * K1Theta, G, Length);
        f64 K2Theta = Step * ThetaFunction(OmegaValues[Index] + 0.5f * K1Omega);
        f64 K3Omega = Step * OmegaFunction(ThetaValues[Index] + 0.5f * K2Theta, G, Length);
        f64 K3Theta = Step * ThetaFunction(OmegaValues[Index] + 0.5f * K2Omega);
        f64 K4Omega = Step * OmegaFunction(ThetaValues[Index] * K3Theta, G, Length);
        f64 K4Theta = Step * ThetaFunction(OmegaValues[Index] * K3Omega);

        OmegaValues[Index+1] = OmegaValues[Index] + (K1Omega + 2 * K2Omega + 2 * K3Omega + K4Omega)/6.0f;
        ThetaValues[Index+1] = ThetaValues[Index] + (K1Theta + 2 * K2Theta + 2 * K3Theta + K4Theta)/6.0f;
    }

    free(OmegaValues);

    return ThetaValues;
}

// NOTE(miha): This function writes results (theta values (position) and time
// interval) to the file 'Filename'. Structure of the file is: first line
// containes all theta values seperated by comma, second line contains time
// interval seperated by comma. 
// CARE(miha): There is an extra comma at the end of each line.
void
WriteResultsToFile(char *Filename, f64 *ThetaValues, f64 Time, u32 NumberOfIntervals)
{
    f64 Step = Time/NumberOfIntervals;

    FILE *File = NULL;

    fopen_s(&File, Filename, "w");

    for(u32 Index = 0; Index < NumberOfIntervals; ++Index)
    {
        fprintf(File, "%f,", ThetaValues[Index]);
    }

    fprintf(File, "\n");

    f64 StepCounter = 0.0f;
    for(u32 Index = 0; Index < NumberOfIntervals; ++Index)
    {
        fprintf(File, "%f,", StepCounter += Step);
    }

    fclose(File);
}

#endif // PENDULUM_H
