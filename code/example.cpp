// NOTE(miha): Include our library.
#include "pendulum.h"

int
main()
{
    // NOTE(miha): Define variables we will use for pendulum.
    f64 Length = 2.0f;
    f64 Time = 40.0f;
    f64 InitialPosition = ToRadians(10.0f);
    f64 InitialVelocity = ToRadians(1.0f);
    u32 NumberOfIntervals = 1000;

    // NOTE(miha): Calculate undamped pendulum differential equation for 'Time'
    // on 'NumberOfIntervals' intervals. Resulting positions are returned in
    // array 'Positions'.
    f64 *Positions = Pendulum(Length, Time, InitialPosition, InitialVelocity, NumberOfIntervals);

    // NOTE(miha): Write resulting positions to the file, so we can import it
    // in the Jupyter Notebook. We do this because it is easier to draw graphs
    // than it is in cpp.
    WriteResultsToFile("results", Positions, Time, NumberOfIntervals);

    free(Positions);

    return 0;
}
